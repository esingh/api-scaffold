import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configurePostgres } from './auth.config';
import { AuthService } from './auth.service';
import { UsersModule } from './users';

@Module({
  imports: [
    ConfigModule,
    UsersModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: configurePostgres,
    }),
  ],
  providers: [AuthService],
  exports: [AuthService],
})
export class AuthModule {}
