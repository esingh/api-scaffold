import jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UserAlreadyExist } from './auth.error';
import { EnvironmentVariables } from './auth.config';
import { SessionIdentity } from './auth.interfaces';
import { UsersService } from './users';

@Injectable()
export class AuthService {
  constructor(
    private configService: ConfigService<EnvironmentVariables>,
    private userService: UsersService,
  ) {}
  async authenticateUser(
    username: string,
    password: string,
    ip: string,
  ): Promise<{ authToken: string }> {
    try {
      const user = await this.userService.login(username, password, ip);
      const authToken = this.generateJwt(
        <SessionIdentity>{
          user: { id: user.id, username: user.username, roles: user.roles },
        },
        'identity',
      );
      return { authToken };
    } catch {
      throw new Error('Invalid credentials');
    }
  }
  async registerUser(
    username: string,
    password: string,
  ): Promise<{ confirmationToken: string }> {
    try {
      const user = await this.userService.create(username, password);
      const confirmationToken = this.generateJwt(
        { user: user.id },
        'confirmation',
      );
      return { confirmationToken };
    } catch {
      throw new UserAlreadyExist();
    }
  }
  generateJwt(payload: any, subject: string) {
    return jwt.sign(
      payload,
      this.configService.get('JWT_SECRET', 'development'),
      {
        subject,
        algorithm: this.configService.get('JWT_ALG', 'HS256'),
        issuer: this.configService.get('JWT_ISSUER', 'auth.service'),
        expiresIn: this.configService.get('JWT_EXPIRE', '2h'),
      },
    );
  }
  verifyJwt(token: string, subject: string) {
    return <SessionIdentity>jwt.verify(token, this.configService.get('JWT_SECRET'), {
      algorithms: ['HS256', 'RS256'],
      subject,
      issuer: this.configService.get('JWT_ISSUER', 'auth.service'),
      maxAge: this.configService.get('JWT_EXPIRE', '4h'),
    });
  }
}
