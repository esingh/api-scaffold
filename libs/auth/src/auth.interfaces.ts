export enum Role {
  User = 'user',
  Admin = 'admin',
}


export interface SessionIdentity {
  user: {
    id: number;
    username: string;
    roles: Role[];
  };
}
