import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { User } from './users';

export interface EnvironmentVariables {
  APP_ENV: string;
  PORT: number;
  PG_HOST: string;
  PG_PORT: number;
  PG_USER: string;
  PG_PASS: string;
  JWT_ALG: string;
  JWT_SECRET: string;
  JWT_ISSUER: string;
  JWT_EXPIRE: string | number;
}

export const configurePostgres = async (
  configService: ConfigService<EnvironmentVariables>,
) =>
  <TypeOrmModuleAsyncOptions>{
    type: 'postgres',
    host: configService.get('PG_HOST'),
    port: configService.get('PG_PORT', 5432),
    username: configService.get('PG_USER'),
    password: configService.get('PG_PASS'),
    database: 'auth',
    schema: configService.get('APP_ENV', 'development'),
    synchronize: configService.get('APP_ENV', 'development') !== 'production',
    entities: [User],
  };
