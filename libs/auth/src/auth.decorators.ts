import {
  SetMetadata,
  createParamDecorator,
  ExecutionContext,
} from '@nestjs/common';
import { Role, SessionIdentity } from './auth.interfaces';

export const Roles = (...roles: Role[]) => SetMetadata('roles', roles);

export const Public = () => SetMetadata('isPublic', true);

export const UserSession = createParamDecorator(
  (_, ctx: ExecutionContext): SessionIdentity => {
    const request = ctx.switchToHttp().getRequest();
    return request.userSession;
  },
);
