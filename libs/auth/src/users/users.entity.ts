import bcrypt from 'bcrypt';
import { Entity, Index, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Role } from '../auth.interfaces';

@Entity()
export class User {
  static readonly bcryptSaltRounds = 10;
  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column({ unique: true, nullable: false })
  username: string;

  @Column({ nullable: false })
  passwordHash: string;

  @Column({ type: 'enum', enum: Role, array: true, default: [Role.User] })
  roles: Role[];

  @Column({
    type: 'timestamptz',
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  lastLoginTimestamp: Date;

  @Column({ nullable: true, default: null })
  lastLoginIp: string;

  @Column({ type: 'timestamptz', nullable: true, default: null })
  confirmationTimestamp: Date;

  async storeCredentials(rawPassword: string) {
    this.passwordHash = await bcrypt.hash(rawPassword, User.bcryptSaltRounds);
  }
  async verifyCredentials(rawPassword: string) {
    return await bcrypt.compare(rawPassword, this.passwordHash);
  }
}
