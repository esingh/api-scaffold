import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection } from 'typeorm';
import { User } from './users.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepo: Repository<User>,
    private connection: Connection,
  ) {}
  async create(username: string, password: string): Promise<User> {
    if (await this.userRepo.findOne({ username })) {
      throw new Error('UserAlreadyExists');
    } else {
      const user = await this.userRepo.preload({ username });
      user.storeCredentials(password);
      await this.userRepo.save(user);
      return user;
    }
  }
  async search(username: string): Promise<User> {
    return await this.userRepo.findOneOrFail({ username });
  }
  async login(username: string, password: string, ip: string): Promise<User> {
    const user = await this.userRepo.findOneOrFail({ username });
    if (user.verifyCredentials(password)) {
      user.lastLoginTimestamp = new Date();
      user.lastLoginIp = ip;
      await this.userRepo.save(user);
      return user;
    } else {
      throw new Error('Invalid credentials');
    }
  }
}
