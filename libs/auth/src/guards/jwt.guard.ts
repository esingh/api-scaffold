import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthService } from '../auth.service';

@Injectable()
export class JwtAuthGuard implements CanActivate {
  constructor(private authService: AuthService, private reflector: Reflector) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>('isPublic', [
      context.getHandler(),
      context.getClass(),
    ]);
    // ? Ignore handler on public routes
    if (isPublic) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    // ? Handle case-insensitive headers
    if (request.headers.authorization) {
      request.headers.Authorization = request.headers.authorization as string;
    }
    // ? Load access claim
    const accessClaim = request.headers.Authorization as string;

    // ? Check Authorization header
    if (!accessClaim || !accessClaim.startsWith('Bearer ')) {
      return false;
    }
    let token: string;
    // ? Check token header format
    try {
      token = accessClaim.split('Bearer ')[1];
    } catch {
      return false;
    }

    // ? Bind user session to request context via access token
    request.identity = await this.authService.verifyJwt(token, 'identity');

    // ? Allow request
    return true;
  }
}
