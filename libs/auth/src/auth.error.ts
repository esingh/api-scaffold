export class UserAlreadyExist extends Error {
  constructor(args?: any) {
    super(...args);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UserAlreadyExist);
    }
    this.name = 'UserAlreadyExist';
  }
}
