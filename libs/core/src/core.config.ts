
export interface EnvironmentVariables {
  APP_ENV: string;
  API_GW_URI: string;
  EVENTS_GW_URI: string;
}
