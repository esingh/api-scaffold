import { Module, Global } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health';
import { CoreService } from './core.service';

@Global()
@Module({
  imports: [ConfigModule, TerminusModule],
  providers: [CoreService],
  controllers: [HealthController],
  exports: [ConfigModule]
})
export class CoreModule {}
