import { Controller, Get } from '@nestjs/common';
import { HttpHealthIndicator, HealthCheckService, HealthCheck, HealthIndicatorFunction } from '@nestjs/terminus';
import { ConfigService } from '@nestjs/config';
import { EnvironmentVariables } from '../core.config'

@Controller('health')
export class HealthController {
  serviceMap;
  constructor(
    private health: HealthCheckService,
    private http: HttpHealthIndicator,
    private configService: ConfigService<EnvironmentVariables>
  ) {
    this.serviceMap = {
      'api-gw': this.configService.get('API_GW_URI'),
      'events-gw': this.configService.get('EVENTS_GW_URI') 
    }
  }

  @Get()
  @HealthCheck()
  check() {
    return this.health.check([
      () => this.http.pingCheck('api-gw', this.serviceMap['api-gw']),
      () => this.http.pingCheck('events-gw', this.serviceMap['events-gw'])
    ]
    )
  }
}
