import { HttpException, HttpStatus } from '@nestjs/common';

export class GeneralServerFailure extends HttpException {
  constructor() {
    super('Server could not resolve request', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
