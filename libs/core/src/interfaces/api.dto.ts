export interface ApiResponse<T = any> {
  data: T;
}

export interface StatusResponse {
  ok: boolean;
}

export interface TokenResponse {
  token: string;
}
