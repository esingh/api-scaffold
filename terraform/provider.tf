provider "aws" {
  profile = "default"
  region  = var.TENANT_REGION
}
