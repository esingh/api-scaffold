# create an IGW (Internet Gateway)
# It enables your vpc to connect to the internet
resource "aws_internet_gateway" "internet_access" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "Internet access"
    Scope       = "networking"
    Environment = "${var.ENVIRONMENT}"
    Tenant      = "${var.TENANT_NAME}"
  }
}

# public subnets can reach to the internet by using this
resource "aws_route_table" "public_crt" {
  vpc_id = aws_vpc.vpc.id
  route {
    // ? associated subnet can reach everywhere
    cidr_block = "0.0.0.0/0"
    // Public cert will use this IGW to reach internet
    gateway_id = aws_internet_gateway.internet_access.id 
  }

  tags = {
    Name        = "Public cert for internet access"
    Scope       = "networking"
    Environment = "${var.ENVIRONMENT}"
    Tenant      = "${var.TENANT_NAME}"
  }
}

# route table association for the public subnets
resource "aws_route_table_association" "crta_public_subnet_1" {
  subnet_id      = aws_subnet.subnet_public_1.id
  route_table_id = aws_route_table.public_crt.id
}

# security group
resource "aws_security_group" "ssh_allowed" {

  vpc_id = aws_vpc.vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = ["${var.TENANT_INGRESS_CIDR}"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Allow SSH access"
    Scope = "security"
  }
}

# static ip for persistent DNS resolution
resource "aws_eip" "static_address" {
  instance = aws_instance.api_server.id

  tags = {
    Name        = "Static IP for API Server"
    Scope       = "networking"
    Environment = "${var.ENVIRONMENT}"
    Tenant      = "${var.TENANT_NAME}"
  }
}