
resource "aws_db_instance" "db" {
  apply_immediately           = false
  allocated_storage           = "${var.TENANT_RDS_STORAGE}"
  max_allocated_storage       = "${var.TENANT_RDS_STORAGE * 1.5}"
  db_subnet_group_name        = "db-subnet-${var.TENANT_NAME}-${var.ENVIRONMENT}"
  engine                      = "postgres"
  engine_version              = "13.2"
  identifier                  = "${var.TENANT_NAME}-${var.ENVIRONMENT}"
  instance_class              = "${var.TENANT_RDS_CLASS}"
  password                    = random_password.password.result
  skip_final_snapshot         = true
  storage_encrypted           = true
  username                    = "ops"
  allow_major_version_upgrade = false
  backup_retention_period     = "${var.TENANT_RDS_RETENTION}"
  maintenance_window          = "${var.TENANT_RDS_MAINTENANCE_WINDOW}"
  deletion_protection         = true

  tags = {
    Name        = "Application Relational Database"
    Scope       = "database"
    Environment = "${var.ENVIRONMENT}"
    Tenant      = "${var.TENANT_NAME}"
  }
}