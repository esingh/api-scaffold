resource "aws_instance" "api_server" {
  ami           = var.TENANT_DEFAULT_AMI
  instance_type = "t2.micro"

  provisioner "local-exec" {
    command = "echo ${aws_instance.api_server.public_ip} > ip_address.txt"
  }

  tags = {
    Name        = "Application API Service Host"
    Scope       = "api"
    Environment = "${var.ENVIRONMENT}"
    Tenant      = "${var.TENANT_NAME}"
  }
}