
variable "ENVIRONMENT" {
  description = "Infrastructure deployment target"
  type        = string
  default     = "production"
}

variable "TENANT_REGION" {
  description = "AWS Region this tenant's resources will be located"
  type        = string
  default     = "us-east-1"
}

variable "TENANT_AZ" {
  description = "VPC availability zone this tenant's resources will be located"
  type        = string
  default     = "us-east-1a"
}

variable "TENANT_DEFAULT_AMI" {
  description = "Default EC2 AMI"
  type        = string
  default     = "ami-0d5eff06f840b45e9"
}

variable "TENANT_RDS_CLASS" {
  description = "Default RDS instance class"
  type        = string
  default     = "db.t3.micro"
}

variable "TENANT_RDS_STORAGE" {
  description = "Default RDS storage size"
  type        = number
  default     = 64
}

variable "TENANT_RDS_RETENTION" {
  description = "RDS backup retention period, in days"
  type        = number
  default     = 30
}

variable "TENANT_RDS_BAKUP_WINDOW" {
  description = "RDS backup window in UTC"
  type        = string
  // ? This can not overlap with the maintenance window
  default = "09:46-10:16"
}

variable "TENANT_RDS_MAINTENANCE_WINDOW" {
  description = "RDS maintenance window in UTC"
  type        = string
  default     = "Mon:00:00-Mon:03:00"
}

variable "TENANT_NAME" {
  description = "Unique tenant identifier"
  type        = string
  default     = "liontech"
}

variable "TENANT_INGRESS_CIDR" {
  description = "Allowed IP range for private direct access"
  type        = string
  // ! CHANGE DEFAULT IN PRODUCTION
  default = "0.0.0.0/0"

}