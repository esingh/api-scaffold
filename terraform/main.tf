terraform {
  required_version = ">= 0.15.4"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
    template = {
      version = "2.2.0"
    }
    random = {
      version = "3.0.1"
    }
  }
}