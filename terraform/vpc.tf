resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  instance_tenancy     = "default"

  tags = {
    Name        = "VPC Network"
    Scope       = "networking"
    Environment = "${var.ENVIRONMENT}"
    Tenant      = "${var.TENANT_NAME}"
  }
}

resource "aws_subnet" "subnet_public_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = var.TENANT_AZ

  tags = {
    Name        = "Public Subnet"
    Scope       = "networking"
    Environment = "${var.ENVIRONMENT}"
    Tenant      = "${var.TENANT_NAME}"
  }
}