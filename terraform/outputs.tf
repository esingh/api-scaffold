output "environment" {
  description = "Infrastructure target"
  value       = var.ENVIRONMENT
}

output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.api_server.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.api_server.public_ip
}

output "instance_static_ip" {
  description = "Static IP address of the EC2 instance"
  value       = aws_eip.static_address.public_ip
}

output "instance_public_dns" {
  description = "Public DNS hostname of the EC2 instance"
  value       = aws_eip.static_address.public_dns
}

output "rds_root_user" {
  description = "Root username of the RDS instance"
  value       = aws_db_instance.db
  sensitive = true
}

output "rds_root_pass" {
  description = "Root username of the RDS instance"
  value       = aws_db_instance.db.password
  sensitive = true
}