# Microservice API ScaffoldS


The code will be saved to `dist`

## Getting Started

Configure your local `.env` file

```bash

cp template.env .env

```

Start development environment

```bash

# Install dependencies
npm install

# Build images
npm start -- --build

# Start services
npm start

# Start w/ Hot-Reload

npm start -- --watch

```


## Documentation

Generate dependency graphs and class descriptions, this makes its easier to understand the code architecture.

```bash

# Build docs

npm run docs:make

# View docs in web browser

npm run docs:view

# browse to apps/ directory or libs/ directory
# select any package and view their docs/ directory

```

## DevOps

### Docker Enviornment

Docker provides a containerized enviornment to work on the application with additional tools for analyzing and debuging.

```bash

# Build images

docker-compose build

# Start containers in the background

docker-compose up -d

# or, to start and view all container logs at once on your console

npm run start

# Stops all containers

docker-compose down

# or,

npm run stop
```

Use these tools to debug any container

```bash
# Stream container logs to console

docker-compose logs -f [service-name]

# i.e

docker-compose logs -f api-gw
docker-compose logs -f pgadmin

# or,

npm run logs -- api-gw
npm run logs -- pgadmin

# Launch shell inside of a container

docker-compose exec api-gw bash # or, sh ; sh has higher compatibility

# i.e

docker-compose exex api-gw sh
docker-compose exec pg-primary bash

```

---

### Dealing with volumes and orphaned containers

Sometimes Docker will not remove containers after a teardown, causing some containers to get orphaned which can lead to the application behaving unpredictably. Here is how to deal with orphans:

```bash
npm start -- --remove-orphans

# or,

npm stop -- --remove-orphans

# or
Tear down development envionment

```bash
# Teardown containers with volumes
npm run stop -- -v

# Teardown just containers, leaves memory intact
npm run stop

```
### Build with docker



```bash
docker build \
  ./Dockerfile.dev \ 
  -t api.runtime:latest \
```

```bash
docker run \
  -v ${PWD}:/app \ 
  -v /app/node_modules --rm \
  -p 80:8000 \
  -it api.runtime:latest
```


## Deployments

To begin deployment you must configure AWS

```
aws configure
```