import { Request } from 'express';
import { Body, HttpStatus, UseGuards } from '@nestjs/common';
import { Controller, HttpCode, Req, Post, Get } from '@nestjs/common';
import {
  ApiResponse,
  StatusResponse,
  GeneralServerFailure,
  TokenResponse,
} from '@lib/core';
import { AuthService, JwtAuthGuard, UserSession } from '@lib/auth';
import { MailerService } from '../mailer/mailer.service';
import { SignUpDto, LoginDto } from './auth.dto';
import {
  PasswordsMismatch,
  UserAlreadyExist,
  InvalidUserCredentials,
  CouldNotHandleRequest,
} from './auth.exceptions';
import { SessionIdentity } from '@lib/auth/auth.interfaces';
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private mailerService: MailerService,
  ) {}
  @Post('signup')
  @HttpCode(HttpStatus.CREATED)
  async signup(@Body() { email, password, confirmPassword }: SignUpDto) {
    if (password !== confirmPassword) {
      throw new PasswordsMismatch();
    } else {
      try {
        // ? Try to create user
        const { confirmationToken } = await this.authService.registerUser(
          email,
          password,
        );
        await this.mailerService.sendUserConfirmationEmail(
          email,
          confirmationToken,
        );
        return <ApiResponse<StatusResponse>>{
          data: { ok: true },
        };
      } catch (err) {
        if (err.name === 'UserAlreadyExist') {
          throw new UserAlreadyExist();
        } else if (err.name === 'CouldNotProcessJob') {
          throw new CouldNotHandleRequest();
        } else {
          throw new GeneralServerFailure();
        }
      }
    }
  }
  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() { email, password }: LoginDto, @Req() req: Request) {
    try {
      const clientIp =
        'X-Forwarded-For' in req.headers
          ? (req.headers['X-Forwarded-For'] as string)
          : req.ip;
      const { authToken } = await this.authService.authenticateUser(
        email,
        password,
        clientIp,
      );
      return <ApiResponse<TokenResponse>>{
        data: { token: authToken },
      };
    } catch {
      throw new InvalidUserCredentials();
    }
  }
  @Get('me')
  @UseGuards(JwtAuthGuard)
  async me(@UserSession() identity: SessionIdentity) {
    return <ApiResponse<SessionIdentity>>{data: identity}
  }
}
