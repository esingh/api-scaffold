import { HttpException, HttpStatus } from '@nestjs/common';

export class PasswordsMismatch extends HttpException {
  constructor() {
    super('Passwords do not match', HttpStatus.BAD_REQUEST);
  }
}

export class UserAlreadyExist extends HttpException {
  constructor() {
    super('There is already a user with this account', HttpStatus.CONFLICT);
  }
}

export class InvalidUserCredentials extends HttpException {
  constructor() {
    super('User credentials are invalid', HttpStatus.UNAUTHORIZED);
  }
}

export class CouldNotHandleRequest extends HttpException {
  constructor() {
    super(
      'Your request could not be handled',
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}
