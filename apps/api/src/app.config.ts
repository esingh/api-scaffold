import { ConfigService } from '@nestjs/config';

export interface EnvironmentVariables {
  REDIS_HOST: string;
  REDIS_PORT: number;
}

export const configureRedis = async (
  configService: ConfigService<EnvironmentVariables>,
) => ({
  redis: {
    host: configService.get('REDIS_HOST'),
    port: configService.get('REDIS_PORT', 6379),
    keyPrefix: 'TaskQueue',
  },
});
