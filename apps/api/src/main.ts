import { NestFactory } from '@nestjs/core';
import * as helmet from 'helmet';
import * as compression from 'compression';
import { AppModule } from './app.module';

async function bootstrap(port: number = 8000) {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.use(compression());
  app.enableCors({ origin: '*' });
  await app.listen(port);
}

bootstrap(+process.env.PORT || undefined);
