import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MailerModule as EmailerModule } from '@nestjs-modules/mailer';
import { BullModule } from '@nestjs/bull';
import { configureMailer } from './mailer.config';
import { MailerService } from './mailer.service';
import { MailerConsumer } from './mailer.consumer';

@Module({
  imports: [
    ConfigModule,
    BullModule.registerQueue({
      name: 'emails',
    }),
    EmailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: configureMailer,
    }),
  ],
  providers: [MailerService, MailerConsumer],
  exports: [MailerService],
})
export class MailerModule {}
