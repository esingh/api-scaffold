import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailerOptions } from '@nestjs-modules/mailer/dist/interfaces/mailer-options.interface';
import { ConfigService } from '@nestjs/config';
import { join } from 'path';

export interface EnvironmentVariables {
  APP_ENV: string;
  SMTP_HOST: string;
  SMTP_USER: string;
  SMTP_PASS: string;
  DEFAULT_MAILER_FROM: string;
  APP_CONFIRMATION_URI: string;
}

export const configureMailer = async (
  configService: ConfigService<EnvironmentVariables>,
): Promise<MailerOptions> => ({
  transport: {
    host: configService.get('SMTP_HOST'),
    secure: false,
    auth: {
      user: configService.get('SMTP_USER'),
      pass: configService.get('SMTP_PASS'),
    },
  },
  preview: configService.get('APP_ENV', 'development') !== 'production',
  defaults: {
    from: configService.get(
      'DEFAULT_MAILER_FROM',
      '"No Reply" <noreply@app.local.dev>',
    ),
  },
  template: {
    dir: join(__dirname, 'templates'),
    adapter: new HandlebarsAdapter(undefined, {
      inlineCssEnabled: true,
    }),
    options: {
      strict: true,
    },
  },
  options: {
    partials: {
      dir: join(__dirname, 'templates/layouts'),
      options: {
        strict: true,
      },
    },
  },
});
