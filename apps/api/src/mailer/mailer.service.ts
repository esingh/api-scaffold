import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { CouldNotProcessJob } from './mailer.error';
@Injectable()
export class MailerService {
  constructor(@InjectQueue('emails') private mailerQueue: Queue) {}
  async sendUserConfirmationEmail(email: string, confirmationToken: string) {
    try {
      await this.mailerQueue.add('userConfirmation', {
        email,
        confirmationToken,
      });
    } catch {
      throw new CouldNotProcessJob();
    }
  }
}
