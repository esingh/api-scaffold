export class CouldNotProcessJob extends Error {
  constructor(args?: any) {
    super(...args);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CouldNotProcessJob);
    }
    this.name = 'CouldNotProcessJob';
  }
}
