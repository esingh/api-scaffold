import { ConfigService } from '@nestjs/config';
import { MailerService as EmailerService } from '@nestjs-modules/mailer';
import { Processor, Process, OnQueueActive } from '@nestjs/bull';
import { Job } from 'bull';
import { EnvironmentVariables } from './mailer.config';

@Processor('emails')
export class MailerConsumer {
  constructor(
    private configService: ConfigService<EnvironmentVariables>,
    private emailerService: EmailerService,
  ) {}
  @OnQueueActive()
  onActive(job: Job) {
    console.log(
      `Processing job ${job.id} of type ${job.name} with data ${job.data}...`,
    );
  }
  @Process()
  async userConfirmation(
    job: Job<{ email: string; confirmationToken: string }>,
  ) {
    console.log('User Confirmation:', job);
    const url = encodeURI(
      `${this.configService.get('APP_CONFIRMATION_URI')}/?confirmationToken=${
        job.data.confirmationToken
      }}`,
    );
    await this.emailerService.sendMail({
      to: job.data.email,
      subject: 'User Confirmation',
      template: 'confirmation',
      context: {
        url,
      },
    });
  }
}
