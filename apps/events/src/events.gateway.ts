import {
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import {UseGuards, Logger} from '@nestjs/common';
import {Socket} from 'socket.io';
import {Server} from 'ws';
import {AuthService} from '@lib/auth';
import { EventsGuard } from './events.guard';

@UseGuards(EventsGuard)
@WebSocketGateway({
  namespace: '/stream',
  transports: ['websocket', 'polling'],
})
export class EventsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  private logger: Logger = new Logger('EventsGateway');

  constructor(
    private readonly authService: AuthService,
  ) {}

  afterInit(server: Server): void {
    return this.logger.log('Socket Events ready to accept connections...');
  }

  handleDisconnect(client: Socket): void {
    return this.logger.log(`Client disconnected: ${client.id}`);
  }

  handleConnection(client: Socket): void {
    return this.logger.log(`Client connected: ${client.id}`);
  }
}