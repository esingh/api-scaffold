import {IoAdapter} from '@nestjs/platform-socket.io';
import * as redis from 'redis';
import * as redisIoAdapter from 'socket.io-redis';

export class RedisIoAdapter extends IoAdapter {
  createIOServer(port: number): any {
    const server = super.createIOServer(port);
    const redisHost = process.env.REDIS_HOST;
    const redisPort = parseInt(process.env.REDIS_PORT);
    const pubClient = redis.createClient(redisPort, redisHost);
    const subClient = pubClient.duplicate();
    // const redisAdapter = redisIoAdapter({
    //   host: redisHost,
    //   port: redisPort,
    //   pubClient,
    //   subClient,
    // });
    // server.adapter(redisAdapter);
    return server;
  }
}