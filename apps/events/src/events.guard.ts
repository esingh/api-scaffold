import { Injectable, CanActivate } from '@nestjs/common';
import { AuthService } from '@lib/auth';


@Injectable()
export class EventsGuard implements CanActivate {

  constructor(private authService: AuthService) {
  }

  async canActivate(
    context: any,
  ): Promise<boolean> {
    const { token } = context.args[0].handshake.query;
    try {
       // ? Bind user session to request context via access token
      const identity = await this.authService.verifyJwt(token, 'identity');
      context.identity = identity;
      // ? Allow request
       return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}