import {Module} from '@nestjs/common';
import {AuthModule} from '@lib/auth';
import {EventsGateway} from './events.gateway';

@Module({
  imports: [
    AuthModule,
  ],
  providers: [EventsGateway],
  exports: [EventsGateway],
})
export class EventsModule {}