import { NestFactory } from '@nestjs/core';
import * as helmet from 'helmet';
import * as compression from 'compression';
import { EventsModule } from './events.module';
import { RedisIoAdapter } from './adapters';

async function bootstrap(port: number = 8010) {
  const app = await NestFactory.create(EventsModule);
  app.use(helmet());
  app.use(compression());
  app.enableCors({ origin: '*' });
  //app.useWebSocketAdapter(new RedisIoAdapter(app));
  await app.listen(port);
}

bootstrap(+process.env.PORT || undefined);
