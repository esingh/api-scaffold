
DOCS_DIR := docs/
DOCS_THEME := material

documentation:
	npx compodoc apps/api/src \
		-p tsconfig.docs.json \
		-n 'API Gateway' \
		--theme ${DOCS_THEME} \
		-d apps/api/${DOCS_DIR}
	npx compodoc apps/events/src \
		-p tsconfig.docs.json \
		-n 'Events Gateway' \
		--theme ${DOCS_THEME} \
		-d apps/events/${DOCS_DIR}
	npx compodoc libs/auth/src \
		-p tsconfig.docs.json \
		-n 'Auth Library' \
		--theme ${DOCS_THEME} \
		-d libs/auth/${DOCS_DIR}
	npx compodoc libs/core/src \
		-p tsconfig.docs.json \
		-n 'Core Library' \
		--theme ${DOCS_THEME} \
		-d libs/core/${DOCS_DIR}
