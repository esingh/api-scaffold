#!/usr/bin/bash

set -e

if [ "$1" = 'dev' ]; then
    exec npm start -- $2 --watch

elif [ "$1" = 'prod' ]; then
    exec npm start -- $2

fi

exec "$@"