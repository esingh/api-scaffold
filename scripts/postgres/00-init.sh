#!/bin/bash
set -e
export PGPASSWORD=$POSTGRES_PASSWORD;
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE USER $APP_USER WITH PASSWORD '$APP_PASS';
  CREATE DATABASE auth;
  CONNECT TO auth;
  CREATE SCHEMA development;
  GRANT ALL PRIVILEGES ON DATABASE auth TO $APP_USER;
  GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA development TO $APP_USER;
EOSQL